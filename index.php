<?php
    require ('assets/class/Autoloader.php');
    Autoloader::register();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Héros php</title>
</head>
<body>

<div class="titre">

<?php

    $form = new BootstrapForm($_POST);
    $merlin = new Personnage('Merlin', 180, 40, 20, 'mage', 'humain');
    $harry = new Personnage('Harry', 180, 40, 20, 'mage', 'humain');

    $legolas = new Archer('Legolas', 180, 40, 20, 'archer', 'elfe');


    $harry->attaquer($merlin);
    $legolas->attaquer($merlin);

    var_dump($merlin, $harry, $legolas);

?>

<form action="#" method="post">
    <?php
        echo $form->input('username');
        echo $form->input('password');
        echo $form->submit();
    ?>
</form>


</div>
    <div class="jeu">
        <div class="perso">    
            <form class="formulaire">
                <label for="nom">Nom</label><br>
                    <input type="text" id='nom' name="name">
                <label for="pv">PV</label><br>
                    <input type="number" id='pv' name="pv">
                <label for="attaque">Attaque</label><br>
                    <input type="number" id='attaque' name="attaque">
                <label for="aefense">Défense</label><br>
                    <input type="number" id='defense' name="defense">
                <label for="mana">Mana</label><br>
                    <input type="number" id='mana' name="mana">
                <label for="classe">Classe</label><br>
                    <input type="text" id='classe' name="classe">
                <label for="race">Race</label><br>
                    <input type="text" id='race' name="race">
                <div class="input">
                    <label for="valider">Valider</label><br>
                        <input type="submit" value="valider" id="valider"><br>
                    <label for="nouveauCombat">Nouveau Combat</label><br>
                        <input type="submit" value="nouveauCombat" id="nouveauCombat">
                </div>
            </form>
        </div>

        <div id="joueur">
        </div>
        <div class="button">
            <input type="submit" value="FIGHT!!!" class="myButton" id="fight">
        </div>
        <div id="resultatCombat">
            <h2> Résultat Combat </h2>
        </div>
    </div>
        <script src="assets/index.js"></script>
        


</body>
</html>