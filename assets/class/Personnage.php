<?php

class Personnage {

    const MA_VIE = 180;

    protected $vie = 180;
    protected $nom;

    public function __construct($nom, $pv, $attaque, $defense, $classe, $race) {
        $this->nom = $nom;
        $this->pv = $pv;
        $this->attaque = $attaque;
        $this->defense = $defense;
        $this->classe = $classe;
        $this->race = $race;
    }

    public function setNom($nom){
        $this->nom = $nom;
    }
    public function getNom(){
        return $this->nom;
    }
    public function getAttaque(){
        return $this->attaque;
    }
    public function getVie(){
        return $this->vie;
    }
    public function crier(){
        echo 'a l attaque';
    } 
    public function regenerer($vie = null){
        if(is_null($vie)) {
            $this->vie = self::MA_VIE;
        } else {
            $this->vie += $vie;
        }
    }
    public function mort(){
        return $this->vie <= 0;
    }
    protected function empecherNegatif(){
        if ($this->vie < 0) {
            $this->vie = 0;
        }
    }
    public function attaquer($cible) {
        $cible->vie -= $this->attaque;
        $cible->empecherNegatif();
    } 
}

?>
