<?php
/**
 * 
 * class Form
 * permet de générer un formulaire rapidement
 */
class Form {
    /**
     * Données utilisés par le formulaire
     *
     * @var array
     */
    private $data;
    /**
     * Tag utilisé pour entourer les champs
     *
     * @var string
     */
    public $surround = 'p';

    /**
     * @param array $data
     */
    public function __construct($data = array()) {
        $this->data = $data;
    }
    /**
     * code html entouré
     *
     * @param [type] $html
     * @return string
     */
    protected function surround($html){
        return "<{$this->surround}>$html</{$this->surround}>";
    }
    /**
     * @param [string] $index index de la valeur a récuperer
     * @return string
     */
    protected function getValue($index) {
        return isset($this->data[$index]) ? $this->data[$index] : null;
    } 
    /**
     * @param [string] $name
     * @return string
     */
    public function input($name){
        return $this->surround('<input type="text" name="' . $name . '" value"' . $this->getValue($name) . '">');
    }
    /**
     *
     * @return string
     */
    public function submit(){
        return $this->surround('<button type="submit">Envoyer</button>');
    }
}

?>